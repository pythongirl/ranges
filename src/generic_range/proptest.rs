use core::cmp::Ordering;
use core::fmt::Debug;
use core::ops::Bound;

use proptest::arbitrary::{any, Arbitrary, StrategyFor};
use proptest::strategy::{Map, Strategy};

use crate::{Domain, GenericRange};

/// Strategy returned by `Arbitrary` implementation
type GenericRangeStrategy<T> =
    Map<(StrategyFor<Bound<T>>, StrategyFor<Bound<T>>), fn((Bound<T>, Bound<T>)) -> GenericRange<T>>;

impl<T: Arbitrary + Debug + Domain + 'static> Arbitrary for GenericRange<T> {
    type Parameters = T::Parameters;

    fn arbitrary_with(_args: Self::Parameters) -> Self::Strategy {
        (any::<Bound<T>>(), any::<Bound<T>>()).prop_map(|(start, end)| match (&start, &end) {
            (Bound::Unbounded, _) | (_, Bound::Unbounded) => GenericRange::new_with_bounds(start, end),
            (Bound::Included(val1), Bound::Included(val2))
            | (Bound::Included(val1), Bound::Excluded(val2))
            | (Bound::Excluded(val1), Bound::Included(val2))
            | (Bound::Excluded(val1), Bound::Excluded(val2)) => match val1.cmp(val2) {
                Ordering::Less => GenericRange::new_with_bounds(start, end),
                Ordering::Equal | Ordering::Greater => GenericRange::new_with_bounds(end, start),
            },
        })
    }

    type Strategy = GenericRangeStrategy<T>;
}
