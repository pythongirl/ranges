use core::fmt::Debug;

use proptest::arbitrary::{any, Arbitrary, StrategyFor};
use proptest::collection::{vec, SizeRange, VecStrategy};
use proptest::strategy::{Map, Strategy};

use crate::{Domain, GenericRange, Ranges};

/// Strategy returned by `Arbitrary` implementation
type RangesStrategy<T> = Map<VecStrategy<StrategyFor<GenericRange<T>>>, fn(Vec<GenericRange<T>>) -> Ranges<T>>;

impl<T: Arbitrary + Debug + Domain + 'static> Arbitrary for Ranges<T> {
    type Parameters = (SizeRange, T::Parameters);

    fn arbitrary_with(args: Self::Parameters) -> Self::Strategy {
        vec(any::<GenericRange<T>>(), args.0).prop_map(|vec| {
            let mut ranges = Ranges::with_capacity(vec.len());

            for range in vec {
                let _ = ranges.insert(range);
            }

            ranges
        })
    }

    type Strategy = RangesStrategy<T>;
}
