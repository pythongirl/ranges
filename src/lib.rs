//! # Ranges
//! This crate provides a generic alternative to core/std ranges, set-operations to work with them
//! and a range set that can efficiently store them with the least amount of memory
//! possible.
//!
//! # Features
//! - `From` implementations for all core/std ranges
//! - open ranges like `(3, 10]`
//! - support for [`RangeBounds<T>`]
//! - iterators (even for unbound ranges when the domain has a minimum)
//! - [`Display`] implementations for single and set ranges (with format argument pass-through)
//! - Operators like `|` and `^` for their respective operation
//! - [`Domain`] implementations for types like [`bool`] and [`char`]
//!
//! [`RangeBounds<T>`]: https://doc.rust-lang.org/stable/core/ops/trait.RangeBounds.html
//! [`Display`]: https://doc.rust-lang.org/stable/core/fmt/trait.Display.html
//! [`Domain`]: trait.Domain.html
//! [`bool`]: trait.Domain.html#impl-Domain-for-bool
//! [`char`]: trait.Domain.html#impl-Domain-for-char

// These are outright wrong and are forbidden to be allowed. This forces correct code.
// If there is indeed a false positive in one of these, they can still be moved to the deny section.
#![forbid(
    clippy::complexity,
    clippy::correctness,
    clippy::perf,
    clippy::style,
    const_err,
    deprecated,
    deprecated_in_future,
    arithmetic_overflow,
    exported_private_dependencies,
    future_incompatible,
    improper_ctypes,
    incomplete_features,
    invalid_value,
    irrefutable_let_patterns,
    macro_use_extern_crate,
    mutable_transmutes,
    no_mangle_const_items,
    no_mangle_generic_items,
    non_ascii_idents,
    non_shorthand_field_patterns,
    nonstandard_style,
    overflowing_literals,
    redundant_semicolons,
    renamed_and_removed_lints,
    rust_2018_compatibility,
    rust_2018_idioms,
    stable_features,
    trivial_bounds,
    trivial_casts,
    trivial_numeric_casts,
    type_alias_bounds,
    unconditional_recursion,
    unknown_crate_types,
    unknown_lints,
    unnameable_test_items,
    unreachable_pub,
    unsafe_code,
    unused,
    unused_comparisons,
    unused_import_braces,
    unused_lifetimes,
    unused_results,
    while_true
)]
// These are denied so one is forced to annotate expected behaviour by allowing it.
#![deny(
    clippy::nursery,
    clippy::pedantic,
    clippy::restriction,
    missing_copy_implementations,
    unreachable_code,
    unused_qualifications,
    variant_size_differences
)]
// These can either not be #[allow()]'d, have false-positives or are development-tools
// of which we only care about in the CI.
#![warn(
    clippy::integer_arithmetic,
    clippy::missing_docs_in_private_items,
    clippy::missing_inline_in_public_items,
    clippy::shadow_reuse,
    clippy::shadow_same,
    missing_debug_implementations,
    missing_docs,
    rustdoc,
    single_use_lifetimes
)]
// note: `use_self` is triggered for unreachable!().
#![allow(
    box_pointers,
    clippy::cargo,
    clippy::implicit_return,
    clippy::missing_inline_in_public_items,
    clippy::redundant_pub_crate,
    clippy::unreachable,
    clippy::use_self,
    meta_variable_misuse
)]
// allow these in tests
#![cfg_attr(
    test,
    allow(
        clippy::as_conversions,
        clippy::decimal_literal_representation,
        clippy::integer_arithmetic,
        clippy::let_underscore_must_use,
        clippy::option_unwrap_used,
        clippy::panic,
        clippy::result_unwrap_used,
        clippy::similar_names,
        clippy::unnecessary_operation,
    )
)]
#![cfg_attr(test, allow(unused_results))]
#![cfg_attr(not(any(test, feature = "arbitrary")), no_std)]
extern crate alloc;

pub use crate::domain::Domain;
pub use crate::generic_range::{
    relation::{Arrangement, Relation},
    GenericRange, OperationResult,
};
pub use crate::ranges::Ranges;

/// Trait and required function to correctly distinguish between discrete and continuous types.
/// Already contains base implementations for all primitive and some `Ord` implementing types.
mod domain;
/// Everything related to a single generic range.
mod generic_range;
/// Range-set and combinatory logic.
mod ranges;

/// Re-exports of optional feature crates, to ease usage with this crate.
#[cfg(any(
    feature = "noisy_float",
    feature = "num-bigint",
    feature = "arbitrary",
    feature = "proptest"
))]
pub mod exports {
    #[cfg(feature = "arbitrary")]
    pub use arbitrary;
    #[cfg(feature = "noisy_float")]
    pub use noisy_float;
    #[cfg(feature = "num-bigint")]
    pub use num_bigint;
    #[cfg(feature = "proptest")]
    pub use proptest;
}
