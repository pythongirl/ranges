use core::cmp::Ordering;
use core::ops::Bound;

use crate::Domain;

impl Domain for Ordering {
    const DISCRETE: bool = true;

    fn predecessor(&self) -> Option<Self> {
        match self {
            Ordering::Less => None,
            Ordering::Equal => Some(Ordering::Less),
            Ordering::Greater => Some(Ordering::Equal),
        }
    }

    fn successor(&self) -> Option<Self> {
        match self {
            Ordering::Less => Some(Ordering::Equal),
            Ordering::Equal => Some(Ordering::Greater),
            Ordering::Greater => None,
        }
    }

    fn minimum() -> Bound<Self> {
        Bound::Included(Ordering::Less)
    }

    fn maximum() -> Bound<Self> {
        Bound::Included(Ordering::Greater)
    }

    fn is_next_to(&self, other: &Self) -> bool {
        match (self, other) {
            // when they're equal
            (Ordering::Less, Ordering::Less)
            | (Ordering::Equal, Ordering::Equal)
            | (Ordering::Greater, Ordering::Greater)
            // or have `Equal` in between
            | (Ordering::Less, Ordering::Greater)
            | (Ordering::Greater, Ordering::Less)
             => false,
            (Ordering::Less, Ordering::Equal)
            | (Ordering::Equal, Ordering::Less)
            | (Ordering::Equal, Ordering::Greater)
            | (Ordering::Greater, Ordering::Equal) => true,
        }
    }

    fn shares_neighbour_with(&self, other: &Self) -> bool {
        match (self, other) {
            // when they're next to `Equal`
            | (Ordering::Equal, _)
            | (_, Ordering::Equal)
            // when they're equal
            | (Ordering::Less, Ordering::Less)
            | (Ordering::Greater, Ordering::Greater) => false,
            (Ordering::Less, Ordering::Greater) | (Ordering::Greater, Ordering::Less) => true,
        }
    }
}

#[cfg(test)]
mod tests {
    use core::cmp::Ordering;

    use crate::Domain;

    #[test]
    fn is_next_to() {
        assert!(!Ordering::Less.is_next_to(&Ordering::Less));
        assert!(Ordering::Less.is_next_to(&Ordering::Equal));
        assert!(!Ordering::Less.is_next_to(&Ordering::Greater));
        assert!(Ordering::Equal.is_next_to(&Ordering::Less));
        assert!(!Ordering::Equal.is_next_to(&Ordering::Equal));
        assert!(Ordering::Equal.is_next_to(&Ordering::Greater));
        assert!(!Ordering::Greater.is_next_to(&Ordering::Less));
        assert!(Ordering::Greater.is_next_to(&Ordering::Equal));
        assert!(!Ordering::Greater.is_next_to(&Ordering::Greater));
    }
    #[test]
    fn shares_neighbour_with() {
        assert!(!Ordering::Less.shares_neighbour_with(&Ordering::Less));
        assert!(!Ordering::Less.shares_neighbour_with(&Ordering::Equal));
        assert!(Ordering::Less.shares_neighbour_with(&Ordering::Greater));
        assert!(!Ordering::Equal.shares_neighbour_with(&Ordering::Less));
        assert!(!Ordering::Equal.shares_neighbour_with(&Ordering::Equal));
        assert!(!Ordering::Equal.shares_neighbour_with(&Ordering::Greater));
        assert!(Ordering::Greater.shares_neighbour_with(&Ordering::Less));
        assert!(!Ordering::Greater.shares_neighbour_with(&Ordering::Equal));
        assert!(!Ordering::Greater.shares_neighbour_with(&Ordering::Greater));
    }
    #[test]
    fn predecessor() {
        assert_eq!(Ordering::Less.predecessor(), None);
        assert_eq!(Ordering::Equal.predecessor(), Some(Ordering::Less));
        assert_eq!(Ordering::Greater.predecessor(), Some(Ordering::Equal));
    }
    #[test]
    fn successor() {
        assert_eq!(Ordering::Less.successor(), Some(Ordering::Equal));
        assert_eq!(Ordering::Equal.successor(), Some(Ordering::Greater));
        assert_eq!(Ordering::Greater.successor(), None);
    }
}
