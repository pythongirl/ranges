use core::ops::Bound;

use noisy_float::types::{n32, n64, r32, r64, N32, N64, R32, R64};

use super::Domain;

impl Domain for N32 {
    const DISCRETE: bool = false;

    #[must_use]
    fn minimum() -> Bound<Self> {
        Bound::Included(n32(core::f32::MIN))
    }

    #[must_use]
    fn maximum() -> Bound<Self> {
        Bound::Included(n32(core::f32::MAX))
    }
}

impl Domain for N64 {
    const DISCRETE: bool = false;

    #[must_use]
    fn minimum() -> Bound<Self> {
        Bound::Included(n64(core::f64::MIN))
    }

    #[must_use]
    fn maximum() -> Bound<Self> {
        Bound::Included(n64(core::f64::MAX))
    }
}

impl Domain for R32 {
    const DISCRETE: bool = false;

    #[must_use]
    fn minimum() -> Bound<Self> {
        Bound::Included(r32(core::f32::MIN))
    }

    #[must_use]
    fn maximum() -> Bound<Self> {
        Bound::Included(r32(core::f32::MAX))
    }
}

impl Domain for R64 {
    const DISCRETE: bool = false;

    #[must_use]
    fn minimum() -> Bound<Self> {
        Bound::Included(r64(core::f64::MIN))
    }

    #[must_use]
    fn maximum() -> Bound<Self> {
        Bound::Included(r64(core::f64::MAX))
    }
}

#[cfg(test)]
mod tests {
    use noisy_float::types::N64;

    use crate::Domain;

    #[test]
    #[should_panic = "method called on continuous type or method not yet implemented"]
    fn is_next_to() {
        let _ = N64::new(0.1).is_next_to(&N64::new(0.2));
    }

    #[test]
    #[should_panic = "method called on continuous type or method not yet implemented"]
    fn elements_between() {
        let _ = N64::new(0.1).shares_neighbour_with(&N64::new(0.2));
    }
}
